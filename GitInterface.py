import os
from time import time


def checkout():
    return os.system("git checkout -b req")

def pull():
    return os.system("git pull origin req")

def pull_request():
    t = time()
    yield os.system("git add .")
    yield os.system(f"git commit -m \"req{t}\"")
    yield os.system("git push origin req")
    # TODO: Will not work
    yield os.system(f"git request-pull \"req{t}\" origin req")

def admin_push(username, password):
    return os.system(f"git push https://{username}:{password}@gitlab.com/YG-alu/rg-temple-library.git:req --all")
