from PySide6.QtWidgets import QDialog, QWidget, QPushButton, QMainWindow, QGridLayout, QVBoxLayout, QHBoxLayout, QLabel

from SearchFrontend import *

class MainWindow(QMainWindow):
    def __init__(self, stacked):
        super().__init__()

        self.parent = stacked

        self.search_book = QPushButton("Search book", self)
        self.search_author = QPushButton("Search author", self)
        self.add_book = QPushButton("Add book", self)

        self.layout = QVBoxLayout()
        self.layout.addWidget(QLabel("Welcome to RG Temple Library Application", self))
        self.layout.addWidget(self.search_book)
        self.layout.addWidget(self.search_author)
        self.layout.addWidget(self.add_book)

        self.search_book.clicked.connect(lambda: self.parent.setCurrentIndex(1))
        self.search_author.clicked.connect(lambda: self.parent.setCurrentIndex(2))
        self.add_book.clicked.connect(lambda: self.parent.setCurrentIndex(3))
        
        self.centralWidget = QWidget()
        self.centralWidget.setLayout(self.layout)
        self.setCentralWidget(self.centralWidget)

