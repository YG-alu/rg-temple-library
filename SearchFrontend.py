from PySide6.QtWidgets import (
    QWidget, QPushButton, QVBoxLayout, QHBoxLayout, QTextEdit, 
    QLineEdit, QRadioButton, QListWidget, QStackedWidget, QLabel,
)
import json

from DatabaseInterface import search, search_name_by_language, search_author_by_language
import GitInterface

class PlaceHolder:
    def __init__(self, s):
        self.s = s

    def text(self):
        return self.s


class Search(QWidget):
    def __init__(self, parent, search):
        super().__init__()

        self.parent = parent
        self.search = search
        
        langs = [
            "English",
            "German",
            "Spanish",
            "Italian",
            "Hindi",
            "Bengali",
            "Russian",
            "French",
            "Finnish",
            "Hungarian",
            "Czech",
            "Chinese",
            "Polish",
            "Tamil",
            "Swedish",
            "All"
        ]

        self.language = ""

        self.loan_status = "Loan"

        self.radio_buttons = []
        self.lang_layout = QVBoxLayout()
        for i in range(len(langs)):
            self.radio_buttons.append(QRadioButton(langs[i], self))
            self.radio_buttons[i].toggled.connect(self.SetLanguage)
            if langs[i] == "All":
                self.radio_buttons[i].toggle()
            self.lang_layout.addWidget(self.radio_buttons[i])

        self.line_edit = QLineEdit()
        self.list = QListWidget()
        self.list.itemClicked.connect(self.GetElementDetails)

        self.search_layout = QVBoxLayout()
        self.search_layout.addWidget(self.line_edit)
        self.search_layout.addWidget(self.list)

        self.line_edit.textChanged.connect(self.Search)

        self.lang_widget = QWidget()
        self.lang_widget.setLayout(self.lang_layout)

        self.search_widget = QWidget()
        self.search_widget.setLayout(self.search_layout)

        self.ls = [["", "", "", "", ""]]

        self.details = QStackedWidget()
        w = QWidget()
        w.setLayout(self.DetailsLayout(PlaceHolder("")))
        self.details.addWidget(w)

        self.hlayout = QHBoxLayout()
        self.hlayout.addWidget(self.lang_widget)
        self.hlayout.addWidget(self.search_widget)
        self.hlayout.addWidget(self.details)

        self.mainwidget = QWidget()
        self.mainwidget.setLayout(self.hlayout)

        self.back = QPushButton("Back", self.parent)
        self.back.clicked.connect(lambda: self.parent.setCurrentIndex(0))

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.back)
        self.layout.addWidget(QLabel("Please choose a language and enter then {} of the book"
                                            .format(self.search), self))
        self.layout.addWidget(self.mainwidget)

        self.setLayout(self.layout)

    def SetLanguage(self, toggle):
        if toggle:
            self.language = self.sender().text()
            try: 
                self.line_edit.clear()
                self.list.clear()
            except: pass # Line edit/list has not yet been created

    def Search(self):
        self.list.clear()

        if self.language == "All":
            if self.search == "name":
                self.ls = search(self.line_edit.text())
            else:
                self.ls = search(self.line_edit.text(), table="*", table2="author")
        else:
            if self.search == "name":
                self.ls = search_name_by_language(self.language, self.line_edit.text())
            else:
                self.ls = search_author_by_language(self.language, self.line_edit.text())

        for i in range(len(self.ls)):
            self.list.insertItem(i, self.ls[i][0])

    def GetElementDetails(self, item):
        w = QWidget()
        w.setLayout(self.DetailsLayout(item))
        self.details.addWidget(w)
        self.details.setCurrentIndex(self.details.currentIndex() + 1)

    def DetailsLayout(self, item):
        ret = QVBoxLayout()
        for i in self.ls:
            if i[0] == item.text():
                details = i

        text = QTextEdit()
        text.setReadOnly(True)
        text.setText(f"Name: {details[0].split('.')[0]}\nAuthor: {details[1]}\nLanguage: {details[2]}\nQuantity: {str(details[3])}\nOn loan: {str(details[4])}")
        ret.addWidget(text)

        self.loan = QPushButton(self.loan_status, self)
        self.loan.clicked.connect(lambda: self.Loan(self.loan, details[0].split('.')[0]))
        self.purchase = QPushButton("Purchase", self)

        ret.addWidget(self.loan)
        ret.addWidget(self.purchase)

        return ret

    def Loan(self, button, name):
        GitInterface.checkout()
        GitInterface.pull()
        json_file = 'DB/'
        json_file += name + ".json"
        with open(json_file, 'r+') as f:
            data = json.load(f)
            data['loan'] = int(data['loan']) + 1
            f.seek(0)
            json.dump(data, f, index=4)
            f.truncate()
        GitInterface.pull_request(f"Loan Book {name}")
        self.loan_staus = "Requested Loan"
