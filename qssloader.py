class QSSLoader:
    def __init__(self):
        pass
    @staticmethod
    def read_qss(qss_file):
        with open(qss_file, 'r', encoding="UTF-8") as f:
            return f.read()
