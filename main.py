#!/usr/bin/python3

from PySide6.QtWidgets import QApplication, QStackedWidget, QLabel, QWidget, QVBoxLayout
from PySide6.QtGui import QPixmap, QMovie
from PySide6.QtCore import Qt
import sys

from MainWindow import MainWindow
from SearchFrontend import Search
from AddBooks import AddBooks
from GitInterface import checkout, pull

import qssloader

if __name__ == '__main__':
    app = QApplication(sys.argv)

    widget = QStackedWidget()
    widget.setWindowTitle("RG Temple Library")
    widget.setWindowIcon(QPixmap("res/icon.png"))

    mainWidget = MainWindow(widget)
    mainWidget.setStyleSheet(qssloader.QSSLoader.read_qss("style.qss"))
    widget.addWidget(mainWidget)

    searchNameWidget = Search(widget, "name")
    widget.addWidget(searchNameWidget)

    searchAuthorWidget = Search(widget, "author")
    widget.addWidget(searchAuthorWidget)

    addBooks = AddBooks(widget)
    widget.addWidget(addBooks)

    #checkout()
    #pull()

    widget.show()

    sys.exit(app.exec())
