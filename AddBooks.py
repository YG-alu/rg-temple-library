from PySide6.QtWidgets import QWidget, QPushButton, QLabel, QLineEdit, QVBoxLayout, QHBoxLayout, QFormLayout, QCheckBox

from DatabaseInterface import add
from GitInterface import admin_push, pull_request

class AddBooks(QWidget):
    def __init__(self, parent):
        self.parent = parent

        super().__init__()

        self.form = QFormLayout()
        self.name = QLineEdit(self)
        self.author = QLineEdit(self)
        self.language = QLineEdit(self)
        self.count = QLineEdit(self)
        self.loan = QLineEdit("No", self)
        self.loan.setReadOnly(True)  # Temp

        self.form.addRow("Name: ", self.name)
        self.form.addRow("Author: ", self.author)
        self.form.addRow("Language: ", self.language)
        self.form.addRow("Quantity: ", self.count)
        self.form.addRow("On loan: ", self.loan)

        self.formwidget = QWidget()
        self.formwidget.setLayout(self.form)

        self.back = QPushButton("Back", self)
        self.back.clicked.connect(lambda: self.parent.setCurrentIndex(0))

        self.l = QLabel("Warning: Please check the information above to make sure it is correct before adding!")
        self.l.setStyleSheet("font-size: 15pt; color: #a00000; font-style: italic;")
        self.l2 = QLabel("A push request will be sent to the maintainer to add this book.\nThis may take time to reappear.\nFurthermore, if there are any problems, please contact ___")
        self.l2.setStyleSheet("font-size: 10pt;")
        self.add = QPushButton("Add", self)
        self.add.clicked.connect(self.AddBook)


        self.adminformwidget = QWidget()
        self.adminform = QFormLayout()
        self.admin_radio = QCheckBox(self)
        self.admin_user = QLineEdit(self)
        self.admin_user.setDisabled(True)
        self.admin_password = QLineEdit(self)
        self.admin_password.setEchoMode(QLineEdit.Password)
        self.admin_password.setDisabled(True)
        self.admin_radio.stateChanged.connect(self.AdminSelected)
        self.adminform.addRow("I am admin", self.admin_radio)
        self.adminform.addRow("User name: ", self.admin_user)
        self.adminform.addRow("Password: ", self.admin_password)
        self.adminformwidget.setLayout(self.adminform)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.back)
        self.layout.addWidget(self.formwidget)
        self.layout.addWidget(self.adminformwidget)
        self.layout.addWidget(self.l)
        self.layout.addWidget(self.l2)
        self.layout.addWidget(self.add)

        self.setLayout(self.layout)
        
    def AddBook(self):
        add(self.name, self.author, self.language, self.count, self.loan)
        if self.admin_radio.isChecked():
            admin_push(self.admin_user.text, self.admin_password.text)
        else:
            pull_request()

    def AdminSelected(self):
        if self.admin_radio.isChecked():
            self.admin_user.setDisabled(False)
            self.admin_password.setDisabled(False)
        else:
            self.admin_user.setDisabled(True)
            self.admin_password.setDisabled(True)
